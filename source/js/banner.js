$(function() {

    var request = null;
    var mouse = {
        x: 0,
        y: 0
    };
    var cx = window.innerWidth/2;
    var cy = window.innerHeight/2;

    $('body').mousemove(function(event) {

        mouse.x = event.pageX;
        mouse.y = event.pageY;

        cancelAnimationFrame(request);
        request = requestAnimationFrame(update);
    });

    function update() {

        dx = mouse.x;
        dy = mouse.y;

        radius = Math.sqrt(Math.pow(cx, 2) - Math.pow(cy, 2))/5;
        tiltx = (dx/radius);
        tilty = (dy/radius);
        TweenLite.to(".js-parallax-container", 1, {
            x: tiltx,
            y: tilty,
            z:1,
            ease: Power2.easeOut
        });
    }

    $(window).resize(function() {
        cx = window.innerWidth / 2;
        cy = window.innerHeight / 2;
    });
});
//
// $('.js-grid').masonry({
//   // options...
//   itemSelector: '.impression-item',
//   columnWidth: 200
// });
